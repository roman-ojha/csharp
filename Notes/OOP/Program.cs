﻿/*
// using System;
// using ClassAndObject; // 01
// using Fields; // 02
// using Properties; // 03
// using AccessModifier; // 04
// using Methods; // 05
// using ListOfCustomClass; // 07
// using PassingCustomTypesAsArgs; // 08
// using StaticMethods; // 09
// using MethodOverloading; // 10
// using DefaultParameters; // 11
// using MethodOverriding; // 12
// using ReturnCustomObject; // 13
// using Inheritance; // 14
// using Virtual_Methods_and_Override_It; // 15
// using Abstract_Class; // 16
// using Constructor; // 19
// using ConstructorToAssignToReadOnlyProperties; // 20
// using CallBaseClassConstructor; // 21
*/
using InterfaceExample;


namespace myProject
{
    class Program
    {
        static void Main(string[] args)
        {
            // all of your c# executable need a Main method
            // this is where program starts 
            Prog.Run(args);

        }
    }
}
