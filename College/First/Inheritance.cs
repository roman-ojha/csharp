namespace Inheritance
{
    class Father
    {
        public void BaseFunction()
        {
            Console.WriteLine("Base Function");
        }
    }
    class Son : Father
    {

    }
}
